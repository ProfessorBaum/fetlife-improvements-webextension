class Picture {
	public readonly pictureId: number;
	public readonly userId: number;

	constructor(url: string) {
		const userId = Utils.getUserIdFromUrl(url);
		const pictureId = Utils.getPictureIdFromUrl(url);
		if (userId === false || pictureId === false) {
			throw 'User id or picture id not found in url';
		}
		this.pictureId = pictureId;
		this.userId = userId;
	}

	async displayIndex() {
		const userPictures = await StorageUtils.getPictures(this.userId);
		const index = userPictures.findIndex(element => element === this.pictureId);
		if (index < 0) {
			console.info('User picture unknown. Maybe it\'s new?');
			await StorageUtils.deletePictures(this.userId);
			return;
		}
		
		const navigationArrow = document.querySelector('aside a[data-hotkey-navigation="39"]');
		if (navigationArrow === null) {
			const msg = 'Element to attach was not found'; // TODO
			console.error(msg);
			throw msg;
		}
		Utils.createPictureIndexElement(navigationArrow.parentElement as Element, (index + 1) + ' / ' + userPictures.length);
	}
}

let picture = new Picture(window.location.href);
picture.displayIndex();

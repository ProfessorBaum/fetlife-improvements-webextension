namespace Utils {
	export function appendCategoryToBlock(parentBlock: ParentNode, elements: Array<string | Node>, name: string) {
		if (elements.length == 0) return;

		parentBlock.append(document.createElement('br'));
		parentBlock.append(createFetishCategory(name));
		parentBlock.append(createFetishBlock(elements));
	}

	export function createFetishBlock(elements: Array<string | Node>) {
		let block = document.createElement('div');
		block.style.marginLeft = "20px";
		block.style.marginTop = "5px";
		elements.forEach(element => block.append(element, ', '));
		(block.lastChild as Text).replaceWith('.');
		return block;
	}

	export function createFetishCategory(name: string) {
		let em = document.createElement('em');
		em.textContent = name;

		let span = document.createElement('span');
		span.className = 'quiet';
		span.append(em);
		return span;
	}

	export function createFetishTitle(block: Element, title: string) {
		let element = document.createElement('h3');
		element.classList.add('bottom');
		element.textContent = title;
		block.insertAdjacentElement('beforebegin', element);
	}

	export function formatFetishList(block: Element) {
		let general: Array<Element> = [];
		let giving: Array<Element> = [];
		let receiving: Array<Element> = [];
		let wearing: Array<Element> = [];
		let watching: Array<Element> = [];
		let everything: Array<Element> = [];

		block.querySelectorAll('a').forEach(element => {
			if (element.nextElementSibling !== null && element.nextElementSibling.tagName === 'SPAN')
			{
				switch (element.nextElementSibling.textContent)
				{
					case '(giving)':
						giving.push(element);
						break;
					case '(receiving)':
						receiving.push(element);
						break;
					case '(wearing)':
						wearing.push(element);
						break;
					case '(watching others wear)':
						watching.push(element);
						break;
					case '(everything to do with it)':
						everything.push(element);
						break;
					default:
						general.push(element);
						general.push(element.nextElementSibling as HTMLSpanElement);
						break;
				}
			}
			else
			{
				general.push(element);
			}
		});

		// Remove all children
		while (block.firstChild !== null) block.removeChild(block.firstChild);
		if (general.length > 0)
		{
			let fetishBlock = createFetishBlock(general);
			fetishBlock.style.marginTop = '16px';
			block.append(fetishBlock);
		}
		appendCategoryToBlock(block, giving, 'Giving');
		appendCategoryToBlock(block, receiving, 'Receiving');
		appendCategoryToBlock(block, wearing, 'Wearing');
		appendCategoryToBlock(block, watching, 'Watching others wear');
		appendCategoryToBlock(block, everything, 'Everything to do with');
	}

	export function createPictureIndexElement(block: Element, content: string) {
		let indexElement = document.createElement('div');
		indexElement.classList.add('fw4', 'ml1', 'gray');
		indexElement.style.textAlign = 'center';
		indexElement.style.marginTop = '0.5rem';
		indexElement.textContent = content;
		
		block.insertAdjacentElement('afterend', indexElement);
	}

	export function getFetishIdFromUrl(url: string) {
		return getWithRegex(/\/fetishes\/([0-9]+)/, url);
	}

	export function getPictureIdFromUrl(url: string) {
		return getWithRegex(/\/pictures\/([0-9]+)/, url);
	}

	export function getUserIdFromUrl(url: string) {
		let result = getWithRegex(/\/users\/([0-9]+)\/(?:pictures|friends(?!\/[0-9]+)|(?:un)?follow|activity)/, url);
		if (result !== false) {
			return result;
		}
		return getWithRegex(/\/conversations\/new\?with=([0-9]+)/, url);
	}

	function getWithRegex(regex: RegExp, haystack: string) {
		const matches = regex.exec(haystack);
		if (matches === null) return false;

		return +matches[1];
	}
}

namespace StorageUtils {
	const FETISH_KEY = 'fetish_cache_';
	const EXPIRE_AFTER = 60*24*60*60*1000; // 60 days
	const PICTURE_KEY = 'pictures_';
	const PICTURE_EXPIRE_KEY = 'pictures_expire';

	export function cleanup() {
		// TODO
	}

	async function cleanupPictureStorageInternal(expireInfo: {[key: number]: number}) {
		let remove = [];
		const now = Date.now();

		for (let userId of Object.keys(expireInfo)) {
			if (expireInfo[+userId] <= now) {
				remove.push(userId);
			}
		}

		if (remove.length > 0) {
			remove.forEach(userId => delete expireInfo[+userId]);
		}
		
		await browser.storage.local.remove(remove.map(userId => PICTURE_KEY + userId));
		return expireInfo;
	}

	export async function deletePictures(profileId: number) {
		let expireInfo = await getPictureExpireRaw();
		expireInfo[profileId] = 0; // mark as to be deleted
		expireInfo = await cleanupPictureStorageInternal(expireInfo);

		let data: {[key: string]: any} = {};
		data[PICTURE_EXPIRE_KEY] = JSON.stringify(expireInfo);
		await browser.storage.local.set(data);
	}

	export function getFetishes(profileId: number) {
		return getSingleOptionalValue<Array<number>>(FETISH_KEY + profileId, []);
	}

	export function getPictures(profileId: number) {
		return getSingleOptionalValue<Array<number>>(PICTURE_KEY + profileId, []);
	}

	async function getPictureExpireRaw() {
		const expireInfo = await getSingleOptionalValue(PICTURE_EXPIRE_KEY, '{}');
		return JSON.parse(expireInfo) as {[key: number]: number};
	}

	export function setFetishes(profileId: number, fetishes: Array<number>) {
		return setData(FETISH_KEY + profileId, fetishes);
	}

	export async function setPictures(profileId: number, pictures: Array<number>) {
		let expireInfo = await getPictureExpireRaw();
		expireInfo[profileId] = Date.now() + EXPIRE_AFTER;
		expireInfo = await cleanupPictureStorageInternal(expireInfo);

		let data: {[key: string]: any} = {};
		data[PICTURE_KEY + profileId] = pictures;
		data[PICTURE_EXPIRE_KEY] = JSON.stringify(expireInfo);
		await browser.storage.local.set(data);
	}

	async function getSingleOptionalValue<T>(key: string, defaultValue: T) {
		const results = await browser.storage.local.get(key);
		if (!results.hasOwnProperty(key)) {
			return defaultValue;
		}

		return results[key] as T;
	}

	function setData<T>(key: string, array: Array<T>) {
		let data: {[key: string]: any} = {};
		data[key] = array;
		return browser.storage.local.set(data);
	}
}

class PictureOverview {
	private mainPictureElement: HTMLElement;
	private observer: MutationObserver;
	private readonly url: string;
	private readonly xPath = '//main[not(ancestor::header)]';

	constructor(url: string) {
		this.url = url;
		this.observer = new MutationObserver(async (mutations, observer) => {
			if (await this.updateData()) {
				observer.disconnect();
			}
		});

		let possibleTargets = document.evaluate(this.xPath, document);
		let target = this.mainPictureElement = possibleTargets.iterateNext() as HTMLElement;
		while (target !== null) {
			this.observer.observe(target, {childList: true, subtree: true});
			target = possibleTargets.iterateNext() as HTMLElement;
		}
	}

	async updateData() {
		const countSpan = this.mainPictureElement.querySelector('header header span');
		if (countSpan === null) {
			return false;
		}

		const pictureCount = +((/[0-9,]+/.exec(countSpan.textContent || '') || [])[0].replace(/,/g, ''));
		let pictureLinks = document.evaluate('.//a[.//img]', this.mainPictureElement, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE);
		if (pictureLinks.snapshotLength < pictureCount)
		{
			return false;
		}

		const userId = Utils.getUserIdFromUrl(this.url);
		if (userId === false) {
			return false;
		}

		let pictures = [];
		for (let i = 0; i < pictureLinks.snapshotLength; i++)
		{
			let pictureLink = pictureLinks.snapshotItem(i) as HTMLAnchorElement;
			let pictureId = Utils.getPictureIdFromUrl(pictureLink.href);
			if (pictureId !== false) {
				pictures.push(pictureId);
			}
		}

		await StorageUtils.setPictures(userId, pictures);

		return true;
	}
}

let overview = new PictureOverview(window.location.href);

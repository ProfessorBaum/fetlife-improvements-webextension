class Profile {
	private static readonly fetishSelector = '#profile > .container p a';
	public readonly id: number;

	constructor(url: string) {
		//const id = Utils.getUserIdFromUrl(url);
		const links = document.querySelectorAll('.container a') as NodeListOf<HTMLAnchorElement>;

		let id: number | false = false;

		for (let link of links) {
			id = Utils.getUserIdFromUrl(link.href);
			if (id !== false) {
				break;
			}
		}

		if (id === false) {
			throw 'User id not found in url';
		}
		this.id = id;
	}

	getFetishes(): Array<number> {
		const links = document.querySelectorAll(Profile.fetishSelector) as NodeListOf<HTMLAnchorElement>;
		let fetishes = [];

		for (let link of links) {
			const fetish = Utils.getFetishIdFromUrl(link.href);
			if (fetish !== false) {
				fetishes.push(fetish);
			}
		}

		return fetishes;
	}

	static getOwnUserId() {
		const scriptBlocks = document.querySelectorAll('script:not([src])') as NodeListOf<HTMLScriptElement>;

		for (let scriptBlock of scriptBlocks) {
			let matches = /FetLife.currentUser.id\s*=\s*([0-9]+)/.exec(scriptBlock.innerText);
			if (matches !== null) {
				return +matches[1];
			}
		}

		return false;
	}

	highlightFetishes(fetishesToHighlight: Array<number>) {
		const links = document.querySelectorAll(Profile.fetishSelector) as NodeListOf<HTMLAnchorElement>;
		for (let link of links) {
			const fetish = Utils.getFetishIdFromUrl(link.href);
			if (fetish !== false && fetishesToHighlight.includes(fetish)) {
				link.style.color = '#cc0000';
			}
		}
	}

	static isProfile() {
		return document.querySelector(this.fetishSelector) != null;
	}

	isOwnUser() {
		return Profile.getOwnUserId() === this.id;
	}

	reorderFetishes() {
		return [
			this.workOnFetishBlockWithName('Into'),
			this.workOnFetishBlockWithName('Curious about', true),
			this.workOnFetishBlockWithName('Soft limit', true, 'Soft limits', false),
			this.workOnFetishBlockWithName('Hard limit', true, 'Hard limits', false)
		];
	}

	saveFetishes() {
		const fetishes = this.getFetishes();
		return StorageUtils.setFetishes(this.id, fetishes);
	}

	private workOnFetishBlockWithName(name: string, title = false, titleName: string | null = null, list = true) {
		// TODO
		let block = document.evaluate('//p[./span[@class="quiet"]/em[contains(., "' + name + '")]]', document).iterateNext() as HTMLParagraphElement | null;
		if (block === null) {
			// TODO
			//throw new Error('Block ' + name + ' could not be found');
			return false;
		}

		if (title) {
			Utils.createFetishTitle(block, titleName === null ? name : titleName);
		}

		if (list) {
			Utils.formatFetishList(block);
		}
		else {
			(block.querySelector('em') as HTMLElement).remove();
		}
		
		return block;
	}
}

if (Profile.isProfile()) {
	let profile = new Profile(window.location.href);
	if (profile.isOwnUser()) {
		profile.saveFetishes();
	} else {
		let ownId = Profile.getOwnUserId();
		if (ownId !== false) {
			StorageUtils.getFetishes(ownId).then(fetishes => profile.highlightFetishes(fetishes));
		}
	}

	profile.reorderFetishes();
}
